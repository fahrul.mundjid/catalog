package com.fahrul.training.microservices.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fahrul.training.microservices.catalog.dao.ProductDao;
import com.fahrul.training.microservices.catalog.entity.Product;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired private ProductDao productDao;

    @GetMapping("/{id}")
    public Product findById(@PathVariable String id){
        return productDao.findById(id)
                .orElse(null);
    }

    @GetMapping("/")
    public Page<Product> findAll(Pageable pageable) {
        return productDao.findAll(pageable);
    }
}
