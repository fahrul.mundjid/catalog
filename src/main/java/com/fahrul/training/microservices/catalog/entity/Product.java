package com.fahrul.training.microservices.catalog.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data @Entity
public class Product {

    @Id
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(min = 3, max = 50)
    private String code;

    @NotNull @NotEmpty @Size(min = 3, max = 50)
    private String name;

    @NotNull @Min(1)
    private BigDecimal price;
}
