package com.fahrul.training.microservices.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.fahrul.training.microservices.catalog.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
